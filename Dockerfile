FROM rocker/geospatial:4.4.0

LABEL source="https://github.com/davetang/learning_docker/blob/main/rstudio/Dockerfile"

MAINTAINER Veronique Thelen

ARG bioc_ver=3.19

# Pour installer des librairies systèmes custom (s'il te dit une fois lancé qu'il en manque)
# RUN apt-get clean all && \
#     apt-get update && \
#     apt-get upgrade -y && \
#     apt-get install -y \
#         libhdf5-dev \
#         libcurl4-gnutls-dev \
#         libssl-dev \
#         libxml2-dev \
#         libpng-dev \
#         libxt-dev \
#         zlib1g-dev \
#         libbz2-dev \
#         liblzma-dev \
#         libglpk40 \
#         libgit2-dev \
#    && apt-get clean all && \
#     apt-get purge && \
#     rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN Rscript -e "install.packages(c('pak'));"
# RUN Rscript -e "BiocManager::install(version = '${bioc_ver}')"

# the rstudio/ path is set for building with GitHub Actions
# COPY --chown=rstudio:rstudio rstudio/rstudio-prefs.json /home/rstudio/.config/rstudio
# COPY --chown=rstudio:rstudio rstudio/.Rprofile /home/rstudio/

WORKDIR /home/rstudio
ADD data/data_exemple.csv  /home/rstudio/data/
